<?php
	Class Home extends CI_Controller {
		public function __construct(){
			parent:: __construct();
			$this->load->library('session');
			$this->load->model('model');
		}

		public function index(){
			$this->load->view("user/headeruser");
			$data['slider'] = $this->model->getDataSlider();
			$data['articles'] = $this->model->getDataArticles();
			$this->load->view("user/view", $data);
			$this->load->view("user/footeruser");
		}
	
				
//----------------------USER------------------------------USER------------------------------USER-----------------------------

		public function form_join(){
			$this->load->view("user/headeruser");
			$this->load->view('user/form_join'); 
			$this->load->view("user/footeruser");
		}

		public function aboutus(){
			$this->load->view("user/headeruser");
			$this->load->view('user/about-us'); 
			$this->load->view("user/footeruser");
		}

		public function activities(){
			$this->load->view("user/headeruser");
			$this->load->view('user/activities'); 
			$this->load->view("user/footeruser");
		}

		public function articles(){
			$this->load->view("user/headeruser");
			$data['articles'] = $this->model->getDataArticles();
			$this->load->view('user/articles', $data); 
			$this->load->view("user/footeruser");
		}

		public function articles_item($id){
			$this->load->view("user/headeruser");
			$data['articles'] = $this->model->get_data_id_articles($id);
			$this->load->view('user/articles-item', $data); 
			$this->load->view("user/footeruser");
		}

		public function contact(){
			$this->load->view("user/headeruser");
			$this->load->view('user/contact-us'); 
			$this->load->view("user/footeruser");
		}

		public function jadwal(){
			$this->load->view("user/headeruser");
			$data['jadwal'] = $this->model->getDataJadwal();
			$this->load->view('user/jadwal', $data); 
			$this->load->view("user/footeruser");
		}

		public function insert(){
			$this->load->helper('form'); 
		    $this->load->library('form_validation'); 
		    $this->load->library('upload');

			$is_submit = $this->input->post('is_submit');

			if(isset($is_submit) && $is_submit == 1){ 
				$fileUpload = array(); 
				$isUpload = FALSE; 
				 
				$config = array( 
				        'upload_path' => './uploads/', 
				        'allowed_types' => 'jpg|jpeg|png', 
				        'max_size' => '512' 
				); 

			} else { 
			      $this->load->view('views/create'); 
			}

			$this->upload->initialize($config); 

			if ($this->upload->do_upload('userfile')){ 
			        $fileUpload = $this->upload->data(); 
			        $isUpload = TRUE;
			}

			if ($isUpload){
				$arrI = array(
					'Nama' => $this->input->post('Nama'),
					'Pekerjaan' => $this->input->post('Pekerjaan'),
					'Institusi' => $this->input->post('Institusi'),
					'Nomor_Telepon' => $this->input->post('Nomor_Telepon'),
					'ID_Line' => $this->input->post('ID_Line'),
					'Kegiatan' => $this->input->post('Kegiatan'),
					'Tanggal_Kegiatan' => $this->input->post('Tanggal_Kegiatan'),
					'Foto' => $fileUpload['file_name']
					);
			$this->model->insertData('people_join',$arrI);
			echo "<script>alert('Data berhasil di input')</script>";
			$this->index();
			} else {
				echo "<script>alert('Error')</script>";
			}
		}

//----------------------ADMIN------------------------------ADMIN------------------------------ADMIN-----------------------------

		public function wpadmin() {   
		  	$data['err_message'] = ""; 
			$this->session->sess_destroy();
			$this->load->view('admin/login', $data); 
			
		} 

		public function panel_admin(){
			if($this->session->userdata('akses')) {
				$this->load->view("admin/headeradmin");
				$this->load->view("admin/admin");
				$this->load->view("admin/footeradmin");
			}
			else {
				redirect('home/wpadmin');
			}
		}

		public function login() //untuk login
		{
			$username = $this->input->post('username');
			$password = $this->input->post('pass');
			$isLogin = $this->model->login_authen($username, $password);
			//$this->session->sess_destroy();
			if ($isLogin == true){
				$this->session->set_userdata('akses',true);
				redirect('home/panel_admin');
			}		
			else{
					$data['err_message'] = "gagal login";
					$this->load->view('user/login', $data);
				}

		}

		public function logout(){
			$this->session->sess_destroy();
			redirect('home/wpadmin');
		}

		public function people_join(){
			$this->load->view("admin/headeradmin");
			$data['people'] = $this->model->getDataPeople();
			$this->load->view('admin/people_join', $data); 
			$this->load->view("admin/footeradmin");
		}
		
		public function input_articles(){
			$this->load->view("admin/headeradmin");
			$this->load->view('admin/input_articles'); 
			$this->load->view("admin/footeradmin");
		}


		public function input_jadwal(){
			$this->load->view("admin/headeradmin");
			$this->load->view('admin/input_jadwal'); 
			$this->load->view("admin/footeradmin");
		}
		
		public function readJadwal(){
				$this->load->view("admin/headeradmin");
				$data['jadwal'] = $this->model->getDataJadwal();
				$this->load->view('admin/view_jadwal', $data); 
				$this->load->view("admin/footeradmin");

			}

		public function insertJadwal(){
				$arrI = array(
					'id_jadwal' => $this->input->post('jadwal'),
					'kegiatan' => $this->input->post('kegiatan'),
					'tanggal' => $this->input->post('tanggal'),
					'waktu' => $this->input->post('waktu'),
					'tempat' => $this->input->post('tempat'));
					
			$this->model->insertData('jadwal',$arrI);
			echo "<script>alert('Data berhasil di input')</script>";
			redirect('home/input_jadwal');
			 
		}

		public function hapusJadwal($id){
		if($this->session->userdata('akses'))
		{
			$this->model->delete_data_jadwal($id);
			redirect('home/readJadwal');
		}else
			{
				redirect('admin/readJadwal');
			}
		}


		public function slider(){
			$this->load->view("admin/headeradmin");
			$this->load->view('admin/slider'); 
			$this->load->view("admin/footeradmin");
		}

		public function insertSlider(){
			$this->load->helper('form'); 
		    $this->load->library('form_validation'); 
		    $this->load->library('upload');

			$is_submit = $this->input->post('is_submit');

			if(TRUE){ 
				$fileUpload = array(); 
				$isUpload = FALSE; 
				 
				$config = array( 
				        'upload_path' => './assets/home/img/slides/', 
				        'allowed_types' => 'jpg|jpeg|png', 
				        'max_size' => '1024' 
				); 

			}
			//  else { 
			//       $this->load->view('views/create'); 
			// }

			$this->upload->initialize($config); 

			if ($this->upload->do_upload('gambar')){ 
			        $fileUpload = $this->upload->data(); 
			        $isUpload = TRUE;
			}

			if ($isUpload){
				$arrI = array(
					'id_slider' => $this->input->post('slider'),
					'header1' => $this->input->post('Header1'),
					'header2' => $this->input->post('Header2'),
					'gambar' => $fileUpload['file_name']);
			$this->model->insertData('slider',$arrI);
			echo "<script>alert('Data berhasil di input')</script>";
			$this->readSlider();
			} else {
				echo "<script>alert('Error')</script>";
			}
		}

		public function insertArticles(){
			$this->load->helper('form'); 
		    $this->load->library('form_validation'); 
		    $this->load->library('upload');

			$is_submit = $this->input->post('is_submit');

			if(TRUE){ 
				$fileUpload = array(); 
				$isUpload = FALSE; 
				 
				$config = array( 
				        'upload_path' => './assets/home/img/articles/', 
				        'allowed_types' => 'jpg|jpeg|png', 
				        'max_size' => '1024' 
				); 

			}
			//  else { 
			//       $this->load->view('views/create'); 
			// }

			$this->upload->initialize($config); 

			if ($this->upload->do_upload('gambar')){ 
			        $fileUpload = $this->upload->data(); 
			        $isUpload = TRUE;
			}

			if ($isUpload){
				$arrI = array(
					'id_articles' => $this->input->post('articles'),
					'judul' => $this->input->post('judul'),
					'konten' => $this->input->post('konten'),
					'gambar' => $fileUpload['file_name']);
			$this->model->insertData('articles',$arrI);
			echo "<script>alert('Data berhasil di input')</script>";
			redirect('home/readArticles');
			} else {
				echo "<script>alert('Error')</script>";
			}
		}

		public function readArticles(){
				$this->load->view("admin/headeradmin");
				$data['articles'] = $this->model->getDataArticles();
				$this->load->view('admin/view_articles', $data); 
				$this->load->view("admin/footeradmin");

			}

		public function readSlider(){
				$this->load->view("admin/headeradmin");
				$data['slider'] = $this->model->getDataSlider();
				$this->load->view('admin/readSlider', $data); 
				$this->load->view("admin/footeradmin");

			}

		public function updateSlider(){
				$this->load->view("admin/headeradmin");
				$data['slider'] = $this->model->getDataSlider();
				$this->load->view('admin/updateSlider', $data); 
				$this->load->view("admin/footeradmin");

			}


		public function editView($id){
		if($this->session->userdata('akses'))
				{
					$data['edit'] = $this->model->get_data_id($id);
					$this->load->view("admin/headeradmin");
					$this->load->view('admin/updateSlider', $data); 
					$this->load->view("admin/footeradmin");
				}else
				{
						redirect('home/readSlider');
				}
		}

		public function editSlider($id){
		if($this->session->userdata('akses'))
	    {
				$target_Path = NULL;
				if ($_FILES['gambar']['name'] != NULL)
				{
					$string = basename( $_FILES['gambar']['name'] );
					$string = str_replace(" ","-", $string);
					$target_Path = $string;
				}
				if ($_FILES['gambar']['name'] == NULL){
					$data = array(
						'HEADER1'=> $this->input->post('header1'),
						'HEADER2'=> $this->input->post('header2'));

					$query = $this->db->where('id_slider', $id);
					$query = $this->db->update('slider', $data);
					if($query)
					{
						$this->session->set_flashdata('suksesslider', '<div class="col-sm-12 alert alert-success fade in">
														 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
														 Slider Berhasil Diupdate</div>');
						redirect ('home/readSlider');
					}
					else
					{
						$this->session->set_flashdata('gagalslider', '<div class="col-sm-12 alert alert-danger fade in">
														 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
														 Slider Gagal Disimpan</div>');
						redirect ('home/editView/$id');
					}
				}
				if($target_Path!=NULL)
				{
					$data = array(
						'HEADER1'=> $this->input->post('header1'),
						'HEADER2'=> $this->input->post('header2'),
						'GAMBAR' => $target_Path);

					$query = $this->db->where('id_slider', $id);
					$query = $this->db->update('slider', $data);

				if(TRUE){ 
				$fileUpload = array(); 
				$isUpload = FALSE; 
				 
				$config = array( 
				        'upload_path' => './assets/home/img/slides/', 
				        'allowed_types' => 'jpg|jpeg|png', 
				        'max_size' => '1024' 
				); 

				}

				$this->upload->initialize($config); 

				if ($this->upload->do_upload('gambar')){ 
				        $fileUpload = $this->upload->data(); 
				        $isUpload = TRUE;
				}

					if($query)
					{
						if ($target_Path != NULL) {
							move_uploaded_file( $_FILES['gambar']['tmp_name'], $target_Path );
						}
						$this->session->set_flashdata('suksesslider', '<div class="col-sm-12 alert alert-success fade in">
														 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
														 Slider Berhasil Diupdate</div>');
						redirect ('home/readSlider');
					}
					else
					{
						$this->session->set_flashdata('gagalslider', '<div class="col-sm-12 alert alert-danger fade in">
														 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
														 Slider Gagal Disimpan</div>');
						redirect ('home/editView/$id');
					}
				}
			}else
			{
					redirect('home/wpadmin');
			}
		}

		public function hapusSlider($id){
		if($this->session->userdata('akses'))
		{
			$this->model->delete_data_slider($id);
			redirect('home/readSlider');
		}else
			{
				redirect('admin/readSlider');
			}
		}

		public function updateArticles($id){
				$this->load->view("admin/headeradmin");
				$data['edit'] = $this->model->get_data_id_articles($id);
				$this->load->view('admin/update_articles', $data); 
				$this->load->view("admin/footeradmin");

			}

			public function editViewArticles($id){
		if($this->session->userdata('akses'))
				{
					$data['edit'] = $this->model->get_data_id_articles($id);
					$this->load->view("admin/headeradmin");
					$this->load->view('admin/update_articles', $data); 
					$this->load->view("admin/footeradmin");
				}else
				{
						redirect('home/readArticles');
				}
		}


		public function editArticles($id){
		if($this->session->userdata('akses'))
	    {
				$target_Path = NULL;
				if ($_FILES['gambar']['name'] != NULL)
				{
					$string = basename( $_FILES['gambar']['name'] );
					$string = str_replace(" ","-", $string);
					$target_Path = $string;
				}
				if ($_FILES['gambar']['name'] == NULL){
					$data = array(
						'judul'=> $this->input->post('judul'),
						'konten'=> $this->input->post('konten'));

					$query = $this->db->where('id_articles', $id);
					$query = $this->db->update('articles', $data);
					if($query)
					{
						$this->session->set_flashdata('suksesslider', '<div class="col-sm-12 alert alert-success fade in">
														 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
														 Artikel Berhasil Diupdate</div>');
						redirect ('home/readArticles');
					}
					else
					{
						$this->session->set_flashdata('gagalslider', '<div class="col-sm-12 alert alert-danger fade in">
														 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
														 Artikel Gagal Disimpan</div>');
						redirect ('home/editViewArticles/$id');
					}
				}
				if($target_Path!=NULL)
				{
					$data = array(
						'judul'=> $this->input->post('judul'),
						'konten'=> $this->input->post('konten'),
						'gambar'=> $target_Path);

					$query = $this->db->where('id_articles', $id);
					$query = $this->db->update('articles', $data);

				if(TRUE){ 
				$fileUpload = array(); 
				$isUpload = FALSE; 
				 
				$config = array( 
				        'upload_path' => './assets/home/img/articles/', 
				        'allowed_types' => 'jpg|jpeg|png', 
				        'max_size' => '1024' 
				); 

				}

				$this->upload->initialize($config); 

				if ($this->upload->do_upload('gambar')){ 
				        $fileUpload = $this->upload->data(); 
				        $isUpload = TRUE;
				}

					if($query)
					{
						if ($target_Path != NULL) {
							move_uploaded_file( $_FILES['gambar']['tmp_name'], $target_Path );
						}
						$this->session->set_flashdata('suksesslider', '<div class="col-sm-12 alert alert-success fade in">
														 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
														 Slider Berhasil Diupdate</div>');
						redirect ('home/readArticles');
					}
					else
					{
						$this->session->set_flashdata('gagalslider', '<div class="col-sm-12 alert alert-danger fade in">
														 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
														 Slider Gagal Disimpan</div>');
						redirect ('home/editViewArticles/$id');
					}
				}
			}else
			{
					redirect('home/wpadmin');
			}
		}

		public function hapusArticles($id){
		if($this->session->userdata('akses'))
		{
			$this->model->delete_data_articles($id);
			redirect('home/readArticles');
		}else
			{
				redirect('admin/readArticles');
			}
		}

		public function hapusPeople($nama){
		if($this->session->userdata('akses'))
		{
			$this->model->delete_data_people($nama);
			redirect('home/people_join');
		}else
			{
				redirect('home/people_join');
			}
		}
	}
?>