
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Input Slider
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
            </div>
            <!-- /.box-header -->
            <?php echo $this->session->flashdata('suksesslider'); ?>
            <!-- form start -->
            <form role="form" method="post" action="<?php echo base_url()?>index.php/home/insertSlider" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputID">ID Slider</label>
                  <input type="text" name="slider" class="form-control" id="inputID" placeholder="ID tidak boleh sama!">
                </div>
                <div class="form-group">
                  <label for="inputHeader1">Header1</label>
                  <input type="text" name="Header1" class="form-control" id="inputHeader1" placeholder="Header1">
                </div>
                 <div class="form-group">
                  <label for="inputHeader2">Header2</label>
                  <input type="text" name="Header2" class="form-control" id="inputHeader2" placeholder="Header2">
                </div>
                <div class="form-group">
                  <label for="inputGambar">Gambar</label>
                  <input type="file" name="gambar" id="inputGambar">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <input type="submit" name="submit" class="btn btn-primary" value="Submit"></input>
              </div>
            </form>
          </div>
          <!-- /.box -->

         
