<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Slider
      </h1>
    </section>

<!-- Main content -->
  <section id="peopleRegistration">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="white-box">
                           
                            <div class="table-responsive">
                            <form method="post" action="<?php echo base_url().'index.php/home/readSlider'?>">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Id_slider</th>
                                            <th>Header1</th>
                                            <th>Header2</th>
                                            <th>Gambar</th>
                                            <th>Action</th>
                                        </tr>                                
                                    </thead>
                                    <tbody>
                                        <?php foreach ($slider as $x) { ?>
                                        <tr>
                                            <td><?php echo $x['id_slider'] ?></td>
                                            <td><?php echo $x['header1'] ?></td>
                                            <td><?php echo $x['header2'] ?></td>
                                            <td><img height="100" width="100" src="<?php echo base_url().'/assets/home/img/slides/'.$x['gambar'] ?>"></img></td>
                                            <td> <?php echo '<a href="'.base_url().'index.php/home/editView/'.$x['id_slider'].'"
                                            role="button" class="btn btn-warning">Edit</a>
                                            <a href="'.base_url().'index.php/home/hapusSlider/'.$x['id_slider'].'"
                                            role="button" class="btn btn-danger">Hapus</a>'; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <br>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>

         
