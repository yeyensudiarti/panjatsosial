<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Jadwal
      </h1>
    </section>

<!-- Main content -->
  <section id="peopleRegistration">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="white-box">
                           
                            <div class="table-responsive">
                            <form method="post" action="<?php echo base_url().'index.php/home/readJadwal'?>">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Id_jadwal</th>
                                            <th>Kegiatan</th>
                                            <th>Tanggal</th>
                                            <th>Waktu</th>
                                            <th>Tempat</th>
                                            <th>Action</th>
                                        </tr>                                
                                    </thead>
                                    <tbody>
                                        <?php foreach ($jadwal as $x) { ?>
                                        <tr>
                                            <td><?php echo $x['id_jadwal'] ?></td>
                                            <td><?php echo $x['kegiatan'] ?></td>
                                            <td><?php echo $x['tanggal'] ?></td>
                                            <td><?php echo $x['waktu'] ?></td>
                                            <td><?php echo $x['tempat'] ?></td>
                                            <td> <?php echo '<a href="'.base_url().'index.php/home/hapusJadwal/'.$x['id_jadwal'].'" role="button" class="btn btn-danger">Hapus</a>'; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <br>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>

         
