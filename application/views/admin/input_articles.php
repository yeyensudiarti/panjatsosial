
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Input Articles
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
            </div>
            <!-- /.box-header -->
            <?php echo $this->session->flashdata('suksesslider'); ?>
            <!-- form start -->
            <form role="form" method="post" action="<?php echo base_url()?>index.php/home/insertArticles" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputID">ID Articles</label>
                  <input type="text" name="articles" class="form-control" id="inputID" placeholder="ID tidak boleh sama!">
                </div>
                <div class="form-group">
                  <label for="Judul">Judul</label>
                  <input type="text" name="judul" class="form-control" id="Judul" placeholder="Judul Artikel">
                </div>
                
                <div class="form-group">
                  <label>Konten</label>
                  <textarea class="form-control" name="konten" rows="15"></textarea>
                </div>
                <div class="form-group">
                  <label for="inputGambar">Gambar</label>
                  <input type="file" name="gambar" id="inputGambar">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <input type="submit" name="submit" class="btn btn-primary" value="Submit"></input>
              </div>
            </form>
          </div>
          <!-- /.box -->

         
