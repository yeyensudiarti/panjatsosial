
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Input Upcoming Events
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
            </div>
            <!-- /.box-header -->
            <?php echo $this->session->flashdata('suksesslider'); ?>
            <!-- form start -->
            <form role="form" method="post" action="<?php echo base_url()?>index.php/home/insertJadwal" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputID">ID Jadwal</label>
                  <input type="text" name="jadwal" class="form-control" id="inputID" placeholder="ID tidak boleh sama!">
                </div>
                <div class="form-group">
                  <label for="Kegiatan">Kegiatan</label>
                  <input type="text" name="kegiatan" class="form-control" id="Kegiatan" ">
                </div>
                <div class="form-group">
                  <label for="Tanggal">Tanggal</label>
                  <input type="text" name="tanggal" class="form-control" id="Tanggal" ">
                </div>
                <div class="form-group">
                  <label for="Waktu">Waktu</label>
                  <input type="text" name="waktu" class="form-control" id="waktu" ">
                </div>
                <div class="form-group">
                  <label for="Tempat">Tempat</label>
                  <input type="text" name="tempat" class="form-control" id="tempat" ">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <input type="submit" name="submit" class="btn btn-primary" value="Submit"></input>
              </div>
            </form>
          </div>
          <!-- /.box -->

         
