<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Articles
      </h1>
    </section>

<!-- Main content -->
  <section id="peopleRegistration">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="white-box">
                           
                            <div class="table-responsive">
                            <!-- <form method="post" action="<?php echo base_url().'index.php/home/view_articles'?>"> -->
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Id_articles</th>
                                            <th>Judul</th>
                                            <th>Konten</th>
                                            <th>Gambar</th>
                                            <th>Action</th>
                                        </tr>                                
                                    </thead>
                                    <tbody>
                                        <?php foreach ($articles as $x) { ?>
                                        <tr>
                                            <td><?php echo $x['id_articles'] ?></td>
                                            <td><?php echo $x['judul'] ?></td>
                                            <td><?php echo $x['konten'] ?></td>
                                            <td><img height="100" width="100" src="<?php echo base_url().'/assets/home/img/articles/'.$x['gambar'] ?>"></img></td>
                                            <td> <?php echo '<a href="'.base_url().'index.php/home/updateArticles/'.$x['id_articles'].'"
                                            role="button" class="btn btn-warning">Edit</a>
                                            <a href="'.base_url().'index.php/home/hapusArticles/'.$x['id_articles'].'"
                                            role="button" class="btn btn-danger">Hapus</a>'; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <br>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>

         
