  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       People Registrations
      </h1>
    </section>

    <!-- Main content -->
  <section id="peopleRegistration">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="white-box">
                           
                            <div class="table-responsive">
                            <form method="post" action="<?php echo base_url().'index.php/home/readDataPeople'?>">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>Pekerjaan</th>
                                            <th>Institusi</th>
                                            <th>Nomor_Telepon</th>
                                            <th>ID_Line</th>
                                            <th>Kegiatan</th>
                                            <th>Tanggal_Kegiatan</th>
                                            <th>Foto</th>
                                            <th>Action</th>
                                        </tr>                                
                                    </thead>
                                    <tbody>
                                        <?php foreach ($people as $x) { ?>
                                        <tr>
                                            <td><?php echo $x['Nama'] ?></td>
                                            <td><?php echo $x['Pekerjaan'] ?></td>
                                            <td><?php echo $x['Institusi'] ?></td>
                                            <td><?php echo $x['Nomor_Telepon'] ?></td>
                                            <td><?php echo $x['ID_Line'] ?></td>
                                            <td><?php echo $x['Kegiatan'] ?></td>
                                            <td><?php echo $x['Tanggal_Kegiatan'] ?></td>
                                            <td><img height="100" width="100" src="<?php echo base_url().'uploads/'.$x['Foto'] ?>"></img></td>
                                             <td> <?php echo '<a href="'.base_url().'index.php/home/hapusPeople/'.$x['Nama'].'"
                                            role="button" class="btn btn-danger">Hapus</a>'; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <br>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
