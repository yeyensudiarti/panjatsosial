<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>OSR! Spread Love</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
	<link rel="stylesheet" href="css/main.css">
    <link href="css/custom.css" rel="stylesheet">
	
	<script src="//use.edgefonts.net/bebas-neue.js"></script>

    <!-- Custom Fonts & Icons -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/icomoon-social.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	
	<script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
	

</head>

    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        

    <header class="navbar navbar-inverse navbar-fixed-top" role="banner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html"><img src="img/logoosr1.png" alt="Basica"></a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?php echo base_url()?>index.php/home">Home</a></li>
                    <li class="active"><a href="about-us.html">About Us</a></li>
                    <li><a href="services.html">Services</a></li>
                    <li><a href="portfolio.html">Portfolio</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pages <i class="icon-angle-down"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="full-width.html">Full Width Page</a></li>
                            <li><a href="#">Dropdown Menu 1</a></li>
                            <li><a href="#">Dropdown Menu 2</a></li>
                            <li><a href="#">Dropdown Menu 3</a></li>
                            <li><a href="#">Dropdown Menu 4</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Terms of Use</a></li>
                        </ul>
                    </li>
                    <li><a href="blog.html">Blog</a></li> 
                    <li><a href="contact-us.html">Contact</a></li>
                </ul>
            </div>
        </div>
    </header><!--/header-->

        <!-- Page Title -->
		<div class="section section-breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>About Us</h1>
					</div>
				</div>
			</div>
		</div>
        
        <div class="section">
	    	<div class="container">
				<div class="row">
				<div class="col-sm-4">
				<img class="img-responsive" src="img/about-us.jpg" alt="About Us">
				</div>
				<div class="col-sm-8">
						<h2>We are a Department in BEM FTIf ITS</h2>
						<h3>Specializing Organization of Social Responsibility</h3>
						<p>
							Departmen Organization Social Responsibility merupakan departemen yang bertanggung jawab dalam menghubungkan mahasiswa FTIf dengan dunia sosial masyarakat dimana selaras dengan salah satu Tri Dharma Perguruan Tinggi, yaitu pengabdian masyarakat. Bekerja sama dengan departemen lainnya, kami berusaha untuk membangun suasana sosial masyarakat terutama dalam lingkup FTIf. Selain itu OSR berfungsi untuk menjembatani dan bekerja sama HMTC dan HMSI dalam melaksanakan program-program yang berkaitan dengan isu sosial. 
								 Program kerja yang ada di OSR diantaranya:
								1. FUSION
								2. SCS
								3. FAST
								4. Sosial forum ftif
						</p>
						<p>
							1. Fusion itu apa ? (General aja, tema dkk)
							2. Ngapain aja kegiatan nya(lebih detail)
							3. Bedanya taun lalu apa
							4. Alasan fusion masih dijalankan taun ini
							5. Apa ada nilai yg pengen ditanemin d fusion kepengurusan taun ini
							6. Tantangan terbesar, bisa dr orang, atau kondisi dr penyelenggaraan fusion
						</p>						
						<h3>Wide range of services</h3>
						<p>
							1. FUSION : FTIf Unity In Social Action” yang berisi serangkaian kegiatan yang berisi pemberdayaan masyarakat di suatu lingkungan tertentu dengan integrasi dan memaksimalkan seluruh potensi yang dimiliki oleh mahasiswa FTIf sebagai bentuk nyata pengabdian masyarakat
							2. Pengajaran di kammit, pengembangam softskill anak2 kammit, mengembangkan kepedulian. Trus baru2 ini ada pelatihan angklung lho.
							3. Tahun lalu itu adalah inisiasi kammit, tahun ini adalah pengembangannya
							4. Karena pengembangan kammit itu nggak hanya setahun dua tahun. Yg namanya pengabdian ini haruslah kontinyu.
							5. Bermanfaat. Sesai dengan tagline presisi bermanfaat. Sebagai mahasiswa kita harus menjadi manfaat untuk orang orang di sekitar kita khususnya di masyarakat sekitar ITS
							6. Resource. Karena selama ini yang berkunjung ke kammit masih sedikit. Kurang koordinasi. Lain kali datang ya... kammit itu wadahnya BEM FTIF bukan punya OSR aja 😊
						</p>
						
						
						
				<div class="clients-logo-wrapper text-center row">
					<div class="col-lg-2 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="img/logos/logo-1.jpg" alt="Client Name"></a></div>
					<div class="col-lg-2 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="img/logos/logo-2.jpg" alt="Client Name"></a></div>
					<div class="col-lg-2 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="img/logos/logo-3.jpg" alt="Client Name"></a></div>
					<div class="col-lg-2 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="img/logos/logo-4.jpg" alt="Client Name"></a></div>
					<div class="col-lg-2 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="img/logos/logo-5.jpg" alt="Client Name"></a></div>
					<div class="col-lg-2 col-md-1 col-sm-3 col-xs-6"><a href="#"><img src="img/logos/logo-6.jpg" alt="Client Name"></a></div>
				</div>						
					</div>
				</div>
			</div>
		</div>

<hr>		

        <div class="section">
	    	<div class="container">
				<div class="row">
					<!-- Team Member -->
					<div class="col-md-4 col-sm-6">
						<div class="team-member">
							<!-- Team Member Photo -->
							<div class="team-member-image"><img src="img/team/1.jpg" alt="Name Surname"></div>
							<div class="team-member-info">
								<ul>
									<!-- Team Member Info & Social Links -->
									<li class="team-member-name">
										Nicolas Freeman
										<!-- Team Member Social Links -->
										<span class="team-member-social">
											<a href="#"><i class="icon-facebook"></i></a>
											<a href="#"><i class="icon-github"></i></a>
											<a href="#"><i class="icon-tumblr"></i></a>
										</span>
									</li>
									<li>Web Developer</li>
								</ul>
							</div>
						</div>
					</div>
					<!-- End Team Member -->
					<div class="col-md-4 col-sm-6">
						<div class="team-member">
							<div class="team-member-image"><img src="img/team/2.jpg" alt="Name Surname"></div>
							<div class="team-member-info">
								<ul>
									<li class="team-member-name">
										Sarah Tipton
										<span class="team-member-social">
											<a href="#"><i class="icon-facebook"></i></a>
											<a href="#"><i class="icon-dribbble"></i></a>
											<a href="#"><i class="icon-tumblr"></i></a>
										</span>
									</li>
									<li>Web Designer</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="team-member">
							<div class="team-member-image"><img src="img/team/3.jpg" alt="Name Surname"></div>
							<div class="team-member-info">
								<ul>
									<li class="team-member-name">
										Luca Grammont
										<span class="team-member-social">
											<a href="#"><i class="icon-facebook"></i></a>
											<a href="#"><i class="icon-dribbble"></i></a>
											<a href="#"><i class="icon-tumblr"></i></a>
										</span>
									</li>
									<li>Project Manager</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

<hr>

        <div class="section">
	        <div class="container">
	        	<div class="row">
	        		<!-- Featured News -->
	        		<div class="col-sm-6 featured-news">
	        			<h2>Latest Blog Posts</h2>
	        			<div class="row">
	        				<div class="col-xs-4"><a href="blog-post.html"><img src="img/blog/1.jpg" alt="Post Title"></a></div>
	        				<div class="col-xs-8">
	        					<div class="caption"><a href="blog-post.html">Lorem ipsum dolor sit amet</a></div>
	        					<div class="date">14 August 2014 </div>
	        					<div class="intro">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et. <a href="blog-post.html">Read more...</a></div>
	        				</div>
	        			</div>
	        			<div class="row">
	        				<div class="col-xs-4"><a href="blog-post.html"><img src="img/blog/2.jpg" alt="Post Title"></a></div>
	        				<div class="col-xs-8">
	        					<div class="caption"><a href="blog-post.html">Sed ut perspiciatis unde omnis</a></div>
	        					<div class="date">23 June 2014 </div>
	        					<div class="intro">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. <a href="blog-post.html">Read more...</a></div>
	        				</div>
	        			</div>
	        			<div class="row">
	        				<div class="col-xs-4"><a href="blog-post.html"><img src="img/blog/3.jpg" alt="Post Title"></a></div>
	        				<div class="col-xs-8">
	        					<div class="caption"><a href="blog-post.html"> Neque porro quisquam est</a></div>
	        					<div class="date">21 April 2014 </div>
	        					<div class="intro">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. <a href="blog-post.html">Read more...</a></div>
	        				</div>
	        			</div>
	        		</div>
	        		<!-- End Featured News -->
	        		<!-- Latest News -->
	        		<div class="col-sm-6 latest-news">
	        			<h2>Latest News</h2>
	        			<div class="row">
	        				<div class="col-sm-12">
	        					<div class="caption"><a href="full-width.html">Donec elementum mi vitae enim fermentum lobortis.</a></div>
	        					<div class="date">16 May 2013 </div>
	        					<div class="intro">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et. <a href="full-width.html">Read more...</a></div>
	        				</div>
	        			</div>
	        			<div class="row">
	        				<div class="col-sm-12">
	        					<div class="caption"><a href="full-width.html">In hac habitasse platea dictumst.</a></div>
	        					<div class="date">14 May 2013 </div>
	        					<div class="intro">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. <a href="full-width.html">Read more...</a></div>
	        				</div>
	        			</div>	
	        			<div class="row">
	        				<div class="col-sm-12">
	        					<div class="caption"><a href="full-width.html"> Nam condimentum laoreet sagittis.</a></div>
	        					<div class="date">14 May 2013 </div>
	        					<div class="intro">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. <a href="full-width.html">Read more...</a></div>
	        				</div>
	        			</div>					
	        		</div>
	        		<!-- End Featured News -->
	        	</div>
	        </div>
	    </div>

		

	    <!-- Footer -->
	    <div class="footer">
	    	<div class="container">
			
		    	<div class="row">
				
		    		<div class="col-footer col-md-4 col-xs-6">
		    			<h3>Contact Us</h3>
		    			<p class="contact-us-details">
	        				<b>Address:</b> 123 Downtown Avenue, Manhattan, New York, United States of America<br/>
	        				<b>Phone:</b> +1 123 45678910<br/>
	        				<b>Fax:</b> +1 123 45678910<br/>
	        				<b>Email:</b> <a href="mailto:info@yourcompanydomain.com">info@yourcompanydomain.com</a>
	        			</p>
		    		</div>				
		    		<div class="col-footer col-md-4 col-xs-6">
		    			<h3>Our Social Networks</h3>
						<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam.</p>
		    			<div>
		    				<img src="img/icons/facebook.png" width="32" alt="Facebook">
		    				<img src="img/icons/twitter.png" width="32" alt="Twitter">
		    				<img src="img/icons/linkedin.png" width="32" alt="LinkedIn">
							<img src="img/icons/rss.png" width="32" alt="RSS Feed">
						</div>
		    		</div>
		    		<div class="col-footer col-md-4 col-xs-6">
		    			<h3>About Our Company</h3>
		    				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci.</p>
		    		</div>

		    	</div>
		    	<div class="row">
		    		<div class="col-md-12">
		    			<div class="footer-copyright">&copy; 2014 <a href="http://www.vactualart.com/portfolio-item/basica-a-free-html5-template/">Basica</a> Bootstrap HTML Template. By <a href="http://www.vactualart.com">Vactual Art</a>.</div>
		    		</div>
		    	</div>
		    </div>
	    </div>

        <!-- Javascripts -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/jquery-1.9.1.min.js"><\/script>')</script>
        <script src="js/bootstrap.min.js"></script>
		
		<!-- Scrolling Nav JavaScript -->
		<script src="js/jquery.easing.min.js"></script>
		<script src="js/scrolling-nav.js"></script>		

    </body>
</html>