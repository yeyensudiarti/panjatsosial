<!DOCTYPE html>
<html>
	<head>
		<title>Form Join</title>
		<style>
			.form{
				padding: 20px 60px;
			}
			table{
				border-collapse: collapse;
				width: 100%;
			}
			th{
				background: #ff6bad;
				font-style: bold;
			}

			#del{
				border-style: none;
				float: right;
			}

			a{
				text-decoration: none;
				font-style: bold;
				color: #550080;
			}
		</style>
	</head>
	<body>
	  <?php include 'header.html'; ?>
	  <div class="form">
		<table>
			<tr>
				<td>
					<tr><h2>Biodata</h2></tr>
					<?php echo form_open_multipart(base_url('index.php/home/insert')); ?>
						<!--helper form, referensi :
								https://www.codeigniter.com/userguide3/helpers/form_helper.html -->
						<tr><input type="text" name="Nama" placeholder="Nama"></tr><br><br>
						<tr><input type="text" name="Pekerjaan" placeholder="Pekerjaan"></tr><br><br>
						<tr><input type="text" name="Institusi" placeholder="Institusi"></tr><br><br>
						<tr><input type="text" name="Nomor_Telepon" placeholder="Nomor Telepon"></tr><br><br>
						<tr><input type="text" name="ID_Line" placeholder="ID Line"></tr><br><br>
						<tr><input type="text" name="Kegiatan" placeholder="Nama kegiatan"></tr><br><br>
						<tr><input type="text" name="Tanggal_Kegiatan" placeholder="Tanggal kegiatan"></tr><br><br>
						<tr> 
					    <td><label for="userfile">Image</label></td> 
					    <td><input type="file" name="userfile" size="20" /></td> 
						</tr>
						<tr> 
						    <td><input type="hidden" name="is_submit" value="1" /></td> 
						    <td colspan="2"><input type="submit" name="submit" value="Submit"></td> 
						</tr> 
					<?php echo form_close(); ?>
				</td>
			</tr>
	  	</table>
	  	<form method="POST" action="<?php echo base_url().'index.php'?>" style="text-align:center; padding-top:30px;">
	  </div>
	</body>
</html>