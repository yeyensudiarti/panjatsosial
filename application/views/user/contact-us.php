
        <!-- Page Title -->
		<div class="section section-breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<h1>Contact Us</h1>
					</div>
				</div>
			</div>
		</div>
        
        <div class="section section-map">


	        		<div class="col-sm-12" style="padding:0;">
	        			<!-- Map -->
	        			<div class="section">
					    	<div class="container">
					        	<div class="row">
									<h3>Sekretariat BEM FTIf ITS</h3>
					        	</div>
					    	</div>
					    </div>
	        			<div id="contact-us-map">
							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1978.8355645024226!2d112.792127758197!3d-7.278210684621508!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x83df57e6a93ef2c2!2sJurusan+Sistem+Informasi+ITS!5e0!3m2!1sid!2sid!4v1496051670880" width="1348" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
	        			</div>

	        			<div class="section">
					    	<div class="container">
					        	<div class="row">
									<h3>Kampung Mitra BEM FTIf</h3>
					        	</div>
					    	</div>
					    </div>
	        			<div id="contact-us-map">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3957.688828583682!2d112.7660762143202!3d-7.276204094748109!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7fa3205ac5fd3%3A0xc0aa9de81c5779ba!2sJojoran+III+E+Dalam%2C+Mojo%2C+Gubeng%2C+Kota+SBY%2C+Jawa+Timur+60285!5e0!3m2!1sid!2sid!4v1496265613646" width="1348" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
	        			</div>
	        			<!-- End Map -->
					</div>


	    </div>

    