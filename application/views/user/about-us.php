

        <!-- Page Title -->
		<div class="section section-breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>About Us</h1>
					</div>
				</div>
			</div>
		</div>
        
        <div class="section">
	    	<div class="container">
				<div class="row">
				<div class="col-sm-4">
				<img class="img-responsive" src="<?php echo base_url()?>assets/home/img/osr.jpg" alt="About Us">
				</div>
				<div class="col-sm-8">
						<h2>We are a Department in BEM FTIf ITS</h2>
						<h3>Organization of Social Responsibility</h3>
						<p>
							Departmen Organization Social Responsibility merupakan departemen yang bertanggung jawab dalam menghubungkan mahasiswa FTIf dengan dunia sosial masyarakat dimana selaras dengan salah satu Tri Dharma Perguruan Tinggi, yaitu pengabdian masyarakat. Bekerja sama dengan departemen lainnya, kami berusaha untuk membangun suasana sosial masyarakat terutama dalam lingkup FTIf. Selain itu OSR berfungsi untuk menjembatani dan bekerja sama HMTC dan HMSI dalam melaksanakan program-program yang berkaitan dengan isu sosial. 
								 
						</p>
						
						<p>Program kerja yang ada di OSR diantaranya:
								1. FUSION
								FUSION : FTIf Unity In Social Action yang berisi serangkaian kegiatan yang berisi pemberdayaan masyarakat di suatu lingkungan tertentu dengan integrasi dan memaksimalkan seluruh potensi yang dimiliki oleh mahasiswa FTIf sebagai bentuk nyata pengabdian masyarakat. Bentuk dari FUSION adalah pengajaran di kammit, pengembangam softskill anak2 kammit, serta mengembangkan kepedulian.
								2. SCS : Social-Development Community School adalah pelatihan sosial masyarakat khusus untuk masyarakat FTIf.
								3. FAST : Adalah penggalangan dana untuk bencana alam.
								4. Sosial forum ftif : Diskusi antar jurusan yang ada di FTIf mengenai sosial masyarakat.
						</p>					
						
											
						
					</div>
				</div>
			</div>
		</div>



