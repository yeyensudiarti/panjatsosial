<!-- Page Title -->
        <div class="section section-breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Upcoming Event </h1>
                    </div>
                </div>
            </div>
        </div>

<!-- Main content -->
  <section id="peopleRegistration">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="white-box">
                           
                            <div class="table-responsive" >
                            <form method="post" action="<?php echo base_url().'index.php/home/readJadwal'?>">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Kegiatan</th>
                                            <th>Tanggal</th>
                                            <th>Waktu</th>
                                            <th>Tempat</th>
                                        </tr>                                
                                    </thead>
                                    <tbody>
                                        <?php foreach ($jadwal as $x) { ?>
                                        <tr>
                                            <td><?php echo $x['kegiatan'] ?></td>
                                            <td><?php echo $x['tanggal'] ?></td>
                                            <td><?php echo $x['waktu'] ?></td>
                                            <td><?php echo $x['tempat'] ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <br>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
<br>
<br>
         
