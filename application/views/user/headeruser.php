<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>OSR! Spread Love</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url()?>assets/home/css/bootstrap.css" rel="stylesheet">
    <!--http://localhost/FPpbw/assets/css/bootstrap.css-->

    <!-- Custom CSS -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/home/css/main.css">
    <link href="<?php echo base_url()?>assets/home/css/custom.css" rel="stylesheet">
	
	<script src="//use.edgefonts.net/bebas-neue.js"></script>

    <!-- Custom Fonts & Icons -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo base_url()?>assets/home/css/icomoon-social.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/home/css/font-awesome.min.css">
	
	<script src="<?php echo base_url()?>assets/home/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
	

</head>

<body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        

    <header class="navbar navbar-inverse navbar-fixed-top" role="banner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html"><img src="<?php echo base_url()?>assets/home/img/logoosr1.png" alt="OSR"></a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?php echo base_url()?>index.php/home">Home</a></li>
                    <li><a href="<?php echo base_url()?>index.php/home/aboutus">About Us</a></li>
                    <li><a href="<?php echo base_url()?>index.php/home/activities">Activities</a></li>
                    <li><a href="<?php echo base_url()?>index.php/home/articles">Articles</a></li>
                    <li><a href="<?php echo base_url()?>index.php/home/contact">Contact</a></li>
                    <li><a href="<?php echo base_url()?>index.php/home/wpadmin">Login</a></li> 
                    
                </ul>
            </div>
        </div>
    </header><!--/header-->