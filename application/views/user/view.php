	
	
    <section id="main-slider" class="no-margin">
        <div class="carousel slide">
            <ol class="carousel-indicators">
            	<?php
            	$enum =1;
            	foreach($slider as $row){
              	echo '<li data-target="#main-slider" data-slide-to="'.$enum.'" class="'; if($enum==1){echo 'active';}; echo '"></li>';
              	$enum+=1;
            	}
          		?>
            </ol>
            <div class="carousel-inner">
            <?php
	          $enum =1;
	          foreach($slider as $row){
	            echo '<div class="item '; if($enum==1){echo 'active';}; echo '"style="background-image: url('.base_url().'assets/home/img/slides/'.$row['gambar'].')">
	                <div class="container">
	                    <div class="row">
	                        <div class="col-sm-12">
	                            <div class="carousel-content centered">
	                                <h2 class="animation animated-item-1">'.$row['header1'].'</h2>
	                                <p class="animation animated-item-2">'.$row['header2'].'</p>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div><!--/.item-->';
	            $enum+=1;
	          }
	        ?>
                <!--/.item-->
            </div><!--/.carousel-inner-->
        </div><!--/.carousel-->
        <a class="prev hidden-xs" href="#main-slider" data-slide="prev">
            <i class="icon-angle-left"></i>
        </a>
        <a class="next hidden-xs" href="#main-slider" data-slide="next">
            <i class="icon-angle-right"></i>
        </a>
    </section><!--/#main-slider-->

	
		<!-- Call to Action Bar -->
	    <div class="section section-dark">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="calltoaction-wrapper">
							<h3>Wanna join <span style="color:#aec62c; text-transform:uppercase;font-size:24px;">OSR</span> activities?</h3> <a href="<?php echo base_url()?>index.php/home/form_join" class="btn btn-orange">Click here!</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Call to Action Bar -->


		<!-- Services -->
        <div class="section section-white">
	        <div class="container">
	        	<div class="row">
	        		<div class="col-md-4 col-sm-6">
	        			<div class="service-wrapper">
		        			<i class="icon-home"></i>
		        			<h3>Where We Work</h3>
		        			<p>Organizational Social Responsibility (OSR) berada di Sekretariat BEM FTIf ITS yang bertempat di Jurusan Sistem Informasi ITS</p>
		        			<a href="<?php echo base_url()?>index.php/home/contact" class="btn">Read more</a>
		        		</div>
	        		</div>
	        		<div class="col-md-4 col-sm-6">
	        			<div class="service-wrapper">
		        			<i class="icon-briefcase"></i>
		        			<h3>What We Do</h3>
		        			<p>Organizational Social Responsibility mempunyai beberapa kegiatan seperti pengajaran rutin setiap hari minggu pagi, pengembangan bisnis dan skill, dan riset. </p>
		        			<a href="<?php echo base_url()?>index.php/home/activities" class="btn">Read more</a>
		        		</div>
	        		</div>
	        		<div class="col-md-4 col-sm-6">
	        			<div class="service-wrapper">
		        			<i class="icon-calendar"></i>
		        			<h3>Upcoming Activities</h3>
		        			<p>Jadwal kegiatan yang akan dilakukan Organizational Social Responsibility dalam waktu dekat akan diberitahukan segera. </p>
		        			<a href="<?php echo base_url()?>index.php/home/jadwal" class="btn">Read more</a>
		        		</div>
	        		</div>
	        	</div>
	        </div>
	    </div>
	    <!-- End Services -->


<hr>

		<!-- Our Portfolio -->	

        <div class="section section-white">
	        <div class="container">
	        	<div class="row">
	
				<div class="section-title">
				<h1>Our Gallery Activities</h1>
				</div>
		
		
			<ul class="grid cs-style-3">
	        	<?php foreach ($articles as $row) { ?>
	        	<div class="col-md-4 col-sm-6">
					<figure>
						<img src="<?php echo base_url()?>/assets/home/img/articles/<?php echo $row['gambar'] ?>" alt="img04" width = "1024" height = "500">
						<figcaption>
							<h3><?php echo $row['judul'] ?></h3>
							
							<a href="<?php echo base_url()?>index.php/home/articles_item/<?php echo $row['id_articles']?>">Take a look</a>
						</figcaption>
					</figure>
	        	</div>	
	        	<?php } ?>
			</ul>
	        	</div>
	        </div>
	    </div>
		<!-- Our Portfolio -->

	    