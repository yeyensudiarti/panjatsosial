

        <!-- Page Title -->
		<div class="section section-breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>Our Activities </h1>
					</div>
				</div>
			</div>
		</div>
		
		
        <div class="section">
	    	<div class="container">
				<div class="row">
				<div class="col-sm-12">
						<h2>Organization of Social Responsibility</h2>
						
						<p>
							Berikut program kerja yang dimiliki OSR.
						</p>
												
					</div>
				</div>
			</div>
		</div>

<hr>			
        
        <div class="section">
	        <div class="container">
	        	<div class="row">
	        		<div class="col-md-4 col-sm-6">
	        			<div class="service-wrapper">
		        			<i class="icon-calendar"></i>
		        			<h3>FUSION: FTIf Unity In Social Action</h3>
		        			<p>Berisi serangkaian kegiatan yang berisi pemberdayaan masyarakat di suatu lingkungan tertentu dengan integrasi dan memaksimalkan seluruh potensi yang dimiliki oleh mahasiswa FTIf sebagai bentuk nyata pengabdian masyarakat. Kegiatannya antara lain pengajaran di kampung mitra, pengembangam softskill anak-anak kampung mitra, dan mengembangkan kepedulian.</p>
		        		</div>
	        		</div>
	        		<div class="col-md-4 col-sm-6">
	        			<div class="service-wrapper">
		        			<i class="icon-briefcase"></i>
		        			<h3>SCS: Sosdev Community School</h3>
		        			<p>Merupakan pelatihan khususnya untuk mahasiswa FTIf dalam bidang sosial masyarakat.</p>
		        		</div>
	        		</div>
	        		<div class="col-md-4 col-sm-6">
	        			<div class="service-wrapper">
		        			<i class="icon-th-large"></i>
		        			<h3>FAST </h3>
		        			<p>dalah penggalangan dana untuk bencana alam.</p>
		        		</div>
	        		</div>
	        		<div class="col-md-4 col-sm-6">
	        			<div class="service-wrapper">
		        			<i class="icon-home"></i>
		        			<h3>Sosial Forum FTIf</h3>
		        			<p>Diskusi antar jurusan yang ada di FTIf mengenai sosial masyarakat.</p>
		        		</div>
	        		</div>
	        	</div>
	        </div>
	    </div>


<hr>
       

<hr>
		
	  

