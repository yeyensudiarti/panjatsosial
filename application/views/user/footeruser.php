<!-- Footer -->
	    <div class="footer">
	    	<div class="container">
			
		    	<div class="row">
				
		    		<div class="col-footer col-md-4 col-xs-6">
		    			<h3>Contact Us</h3>
		    			<p class="contact-us-details">
	        				<b>Address:</b> Jl. Raya ITS Jurusan Sistem Informasi, Sekretariat BEM FTIf.<br/>
	        				<b>Phone:</b> +681212035205 <br/>
	        				<b>Email:</b> <a href="mailto:osrbemftif@gmail.com">osrbemftif@gmail.com</a>
	        			</p>
		    		</div>				
		    		<div class="col-footer col-md-4 col-xs-6">
		    			<h3>Our Social Media</h3>
						<p>BEM Fakultas Teknologi Informasi mempunyai beberapa sosial media yang bisa langsung dituju  </p>
		    			<div>
		    				<a href="https://id-id.facebook.com/BEM.FTIF.ITS/">
		    				<img src="<?php echo base_url()?>assets/home/img/icons/facebook.png" width="32" alt="Facebook"></a>

		    				<a href="https://www.instagram.com/bemftif_its/">
		    				<img src="<?php echo base_url()?>assets/home/img/icons/instagram.png" width="32" alt="Instagram">

		    				<a href="https://twitter.com/BEMFTIF_ITS">
		    				<img src="<?php echo base_url()?>assets/home/img/icons/twitter.png" width="32" alt="Twitter"></a>

						</div>
		    		</div>
		    		<div class="col-footer col-md-4 col-xs-6">
		    			<h3>About Our Company</h3>
		    				<p>Fakultas Teknologi Informasi mempunyai 2 jurusan yaitu Sistem Informasi dan Teknik Informatika. FTIf mempunyai Badan Eksekutif Mahasiswa yang mempunyai 8 departement, salah satunya adalah Organizational Social Responsibility (OSR). </p>
		    		</div>

		    	</div>
		    </div>
	    </div>

        <!-- Javascripts -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="assets/home/js/jquery-1.9.1.min.js"><\/script>')</script>
        <script src="<?php echo base_url()?>assets/home/js/bootstrap.min.js"></script>
		
		<!-- Scrolling Nav JavaScript -->
		<script src="<?php echo base_url()?>assets/home/js/jquery.easing.min.js"></script>
		<script src="<?php echo base_url()?>assets/home/js/scrolling-nav.js"></script>		

    </body>
</html>