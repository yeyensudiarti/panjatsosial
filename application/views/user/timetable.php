<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>OSR! Spread Love</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url()?>assets/home/css/bootstrap.css" rel="stylesheet">
    <!--http://localhost/FPpbw/assets/css/bootstrap.css-->

    <!-- Custom CSS -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/home/css/main.css">
    <link href="<?php echo base_url()?>assets/home/css/custom.css" rel="stylesheet">
	
	<script src="//use.edgefonts.net/bebas-neue.js"></script>

    <!-- Custom Fonts & Icons -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo base_url()?>assets/home/css/icomoon-social.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/home/css/font-awesome.min.css">
	
	<script src="<?php echo base_url()?>assets/home/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
	

</head>

<body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        

    <header class="navbar navbar-inverse navbar-fixed-top" role="banner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html"><img src="<?php echo base_url()?>assets/home/img/logoosr1.png" alt="OSR"></a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="<?php echo base_url()?>index.php/home">Home</a></li>
                    <li><a href="<?php echo base_url()?>index.php/home/aboutus">About Us</a></li>
                    <li><a href="<?php echo base_url()?>index.php/home/activities">Activities</a></li>
                    <li><a href="<?php echo base_url()?>index.php/home/articles">Articles</a></li>
                    <li><a href="<?php echo base_url()?>index.php/home/contact">Contact</a></li>
                    <li><a href="<?php echo base_url()?>index.php/home/wpadmin">Login</a></li> 
                    
                </ul>
            </div>
        </div>
    </header><!--/header-->








    <!-- Footer -->
        <div class="footer">
            <div class="container">
            
                <div class="row">
                
                    <div class="col-footer col-md-4 col-xs-6">
                        <h3>Contact Us</h3>
                        <p class="contact-us-details">
                            <b>Address:</b> Jl. Raya ITS Jurusan Sistem Informasi, Sekretariat BEM FTIf.<br/>
                            <b>Phone:</b> +681212035205 <br/>
                            <b>Email:</b> <a href="mailto:osrbemftif@gmail.com">osrbemftif@gmail.com</a>
                        </p>
                    </div>              
                    <div class="col-footer col-md-4 col-xs-6">
                        <h3>Our Social Media</h3>
                        <p>BEM Fakultas Teknologi Informasi mempunyai beberapa sosial media yang bisa langsung dituju  </p>
                        <div>
                            <a href="https://id-id.facebook.com/BEM.FTIF.ITS/">
                            <img src="assets/home/img/icons/facebook.png" width="32" alt="Facebook"></a>

                            <a href="https://www.instagram.com/bemftif_its/">
                            <img src="assets/home/img/icons/instagram.png" width="32" alt="Instagram">

                            <a href="https://twitter.com/BEMFTIF_ITS">
                            <img src="assets/home/img/icons/twitter.png" width="32" alt="Twitter"></a>

                        </div>
                    </div>
                    <div class="col-footer col-md-4 col-xs-6">
                        <h3>About Our Company</h3>
                            <p>Fakultas Teknologi Informasi mempunyai 2 jurusan yaitu Sistem Informasi dan Teknik Informatika. FTIf mempunyai Badan Eksekutif Mahasiswa yang mempunyai 8 departement, salah satunya adalah Organizational Social Responsibility (OSR). </p>
                    </div>

                </div>
            </div>
        </div>

        <!-- Javascripts -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="assets/home/js/jquery-1.9.1.min.js"><\/script>')</script>
        <script src="<?php echo base_url()?>assets/home/js/bootstrap.min.js"></script>
        
        <!-- Scrolling Nav JavaScript -->
        <script src="<?php echo base_url()?>assets/home/js/jquery.easing.min.js"></script>
        <script src="<?php echo base_url()?>assets/home/js/scrolling-nav.js"></script>      

    </body>
</html>