

        <!-- Page Title -->
		<div class="section section-breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>Our Portfolio</h1>
					</div>
				</div>
			</div>
		</div>
		
		
        <div class="section">
	    	<div class="container">
				<div class="row">
				<div class="col-sm-12">
						<h2>We are leading company</h2>
						<h3>Specializing in Wordpress Theme Development</h3>
						<p>
							Donec elementum mi vitae enim fermentum lobortis. In hac habitasse platea dictumst. Ut pellentesque, orci sed mattis consequat, libero ante lacinia arcu, ac porta lacus urna in lorem. Praesent consectetur tristique augue, eget elementum diam suscipit id. Donec elementum mi vitae enim fermentum lobortis. 
						</p>
					
					</div>
				</div>
			</div>
		</div>		
        
        <div class="section">
	    	<div class="container">
				<div class="row">
			
			<ul class="grid cs-style-2">
				<?php foreach ($articles as $row) { ?>
	        	<div class="col-md-4 col-sm-6">
					<figure>
						<img src="<?php echo base_url()?>/assets/home/img/articles/<?php echo $row['gambar'] ?>" alt="img04">
						<figcaption>
							<h3><?php echo $row['judul'] ?></h3>
							
							<a href="<?php echo base_url()?>index.php/home/articles_item/<?php echo $row['id_articles']?>">Take a look</a>
						</figcaption>
					</figure>
	        	</div>	
	        	<?php } ?>
			
			</ul>

				
				
			</div>
		</div>
	</div>
