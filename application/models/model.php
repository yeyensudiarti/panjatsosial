<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model extends CI_Model {

	public function index(){
			$this->load->view("user/view");
		}
	
	public function login_authen($username, $password)
	{
		$this->db->select('*');
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		$this->db->from('admin');
		//$this->db->query('SELECT * FROM user WHERE username = '.$username.' && password = '.$password);
		$query = $this->db->get();
		if ($query->num_rows() == 1) return true;
		else return false;
	}

	public function insertData($tableName, $data)
	{
		$dataI = $this->db->insert($tableName,$data);
	}

	public function getDataPeople(){
		return $this->db->get('people_join')->result_array();
	}

	public function getDataSlider(){
		return $this->db->get('slider')->result_array();
	}

	public function getDataArticles(){
		return $this->db->get('articles')->result_array();
	}

	public function getDataJadwal(){
		return $this->db->get('jadwal')->result_array();
	}

	public function get_data_id($id)
	{
		$query = $this->db->get_where('slider', array('id_slider' => $id));
		return $query->row_array();
	}

	public function get_data_id_articles($id)
	{
		$query = $this->db->get_where('articles', array('id_articles' => $id));
		return $query->row_array();
	}

	public function get_data_id_jadwal($id)
	{
		$query = $this->db->get_where('jadwal', array('id_jadwal' => $id));
		return $query->row_array();
	}

	public function delete_data_slider($id)
	{
		return $this->db->delete('slider', array('id_slider'=>$id));
	}

	public function delete_data_jadwal($id)
	{
		return $this->db->delete('jadwal', array('id_jadwal'=>$id));
	}

	public function delete_data_articles($id)
	{
		return $this->db->delete('articles', array('id_articles'=>$id));
	}

	public function delete_data_people($nama)
	{
		return $this->db->delete('people_join', array('nama'=>$nama));
	}
}